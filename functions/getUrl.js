/**/

const express = require('express');
const app = express();
const PORT = process.env.PORT || 5000;

app.get('/:a', (req, res) => {
   
    res.json({
        'lala':req.params.a,
        'port' : PORT
    });

})

app.listen(PORT, ()=>{console.log(`Servidor corriendo en el puerto ${PORT}`);});
/**/
